"""Download header information from Flywheel and write it to a JSON file."""

#!/usr/bin/env python
import argparse
import json
import logging
from pathlib import Path
from typing import Union
from urllib.parse import urlparse

import flywheel
import requests
import yaml

logging.basicConfig(level=logging.INFO)


# Function to load keep_tags from a remote YAML file
def fetch_and_load_keep_tags(source: str):
    """Fetch a remote YAML file and load keep_tags from it.

    Args:
        source (str): The URL of the YAML file to fetch. Could also be a local file path.

    Returns:
        list: A list of keep_tags.
    """
    # Check if source is a URL
    parsed_source = urlparse(source)
    if parsed_source.scheme in ["http", "https"]:
        # Handle remote URL
        try:
            response = requests.get(source, timeout=10)
            response.raise_for_status()  # Raise an exception for HTTP errors
            data = yaml.safe_load(response.text)
        except requests.exceptions.RequestException as e:
            logging.error(f"Failed to fetch YAML file from {source}: {e}")
            raise
    else:
        # Handle local file path
        try:
            with open(source, "r") as file:
                data = yaml.safe_load(file)
        except Exception as e:
            logging.error(f"Failed to load YAML file from {source}: {e}")
            raise
    keep_tags = [
        field["name"]
        for field in data.get("dicom", {}).get("fields", [])
        if (field.get("keep", False) or not field.get("remove", True))
    ]
    return keep_tags


def safe_get(d: dict, keys: list, default: str) -> Union[dict, str]:
    """Safely navigate through nested dictionaries using a list of keys.

    Args:
        d (dict): The dictionary to navigate.
        keys (list): A list of keys to navigate through the dictionary.
        default (str): The default value to return if a key is not found.

    Returns:
        Union[dict, str]: The value found at the nested key, or the default value.
    """
    try:
        for key in keys:
            d = d[key]
        return d
    except KeyError:
        return default


def get_file_url(header: dict, parents: dict, api_key: str) -> str:
    """Generate the file URL based on header and parent information.

    Args:
        header (dict): The file header information.
        parents (dict): The file parent information.
        api_key (str): The Flywheel API key.

    Returns:
        str: The generated URL or a default string if unable to find the URL.
    """
    study_uid = safe_get(
        header, ["info", "header", "dicom", "StudyInstanceUID"], "empty"
    )
    series_uid = safe_get(
        header, ["info", "header", "dicom", "SeriesInstanceUID"], "empty"
    )
    project_id = parents.get("project", "empty")
    base_url = api_key.split(":")[0]

    if any(value == "empty" for value in [study_uid, series_uid, project_id]):
        return "unable_to_find_file"

    return (
        f"{base_url}/ohif-viewer/project/{project_id}/viewer/{study_uid}/{series_uid}"
    )


def initialize_flywheel_client(api_key):
    """Initialize the Flywheel client.

    Args:
        api_key (str): The Flywheel API key.

    Returns:
        Flywheel client or None if initialization failed.
    """
    fw = flywheel.Client(api_key)
    if not fw:
        logging.error("Failed to initialize Flywheel client")
        return None
    return fw


def build_file_header(file, api_key, keep_tags):
    """Build a file header based on file information and an API key.

    Args:
        file: The Flywheel file object.
        api_key (str): The Flywheel API key.
        keep_tags (list): A list of tags to keep in the file header.

    Returns:
        dict: The built file header.
    """
    info = file.get("info", {})
    header = {
        "info": info,
        "type": file.type,
        "file_id": file.file_id,
        "name": file.name,
        "url": get_file_url({"info": info}, file.parents, api_key),
        "zip_member_count": file.get("zip_member_count"),
    }
    return clean_file_header(header, keep_tags)


def clean_file_header(data_original: dict, keep_tags) -> dict:
    """Clean the original file header by retaining only necessary keys.

    Args:
        data_original (dict): The original file header data.
        keep_tags (list): A list of tags to keep in the file header.

    Returns:
        dict: The cleaned file header.
    """
    if data_original["info"].get("header"):
        data_new = {
            key: data_original["info"]["header"]["dicom"].get(key)
            for key in keep_tags
            if key in data_original["info"]["header"]["dicom"]
        }
        data_new = {"info": {"header": {"dicom": data_new}}}
        data_new["file_id"] = data_original["file_id"]
        data_new["url"] = data_original["url"]
        data_new["zip_member_count"] = data_original["zip_member_count"]
        data_new["type"] = data_original["type"]
        data_new["name"] = data_original["name"]
        return data_new
    return data_original


def fetch_file_info(
    api_key,
    output_dir,
    keep_tags,
    file_path=None,
    session_id=None,
    file_name=None,
):  # noqa: PLR0913
    """Fetch file information by project name, and write it to a JSON file. Requires either file_path or session_id and file_name.

    Args:
        api_key (str): The Flywheel API key.
        output_dir (str): The directory to output the JSON file with file information.
        keep_tags (list): A list of tags to keep in the file header.
        file_path (str, optional): The Flywheel path of the file to fetch info for.
        session_id (str, optional): The Flywheel session ID.
        file_name (str, optional): The name of the file in Flywheel.
    """
    # pylint: disable=too-many-locals
    fw = initialize_flywheel_client(api_key)
    if fw is None:
        return

    found_file = None

    if session_id and file_name:
        # Get the session using the session ID
        session = fw.get(session_id)

        # Loop through all acquisitions in the session
        for acquisition in session.acquisitions():
            # Refresh acquisition to get the files
            acquisition = acquisition.reload()

            # Search for the file by name
            for file in acquisition.files:
                if file.name == file_name:
                    found_file = file
                    break

            if found_file:
                break

        if not found_file:
            logging.error(f"File '{file_name}' not found in session '{session_id}'.")
            return
    elif file_path:
        found_file = fw.lookup(file_path)

    if not found_file:
        logging.error("No file found with provided file_path.")
        return

    # Proceed with file processing
    file = found_file.reload()  # Ensure the file object is fully loaded
    file_name = file.name
    header = build_file_header(file, api_key, keep_tags)
    json_file_path = Path(f"{output_dir}/{file_name}_{file.file_id}.json")
    logging.info(f"Writing file header to {json_file_path}")
    Path(output_dir).mkdir(parents=True, exist_ok=True)
    json_file_path.write_text(json.dumps(header, indent="\t"), encoding="UTF-8")
    return


def main(
    api_key,
    output_dir,
    deid_profile_path=None,
    file_path=None,
    session_id=None,
    file_name=None,
):  # noqa: PLR0913
    """Main function to run the fetching operation."""
    # Initialize keep_tags from the remote deid.yaml profile
    logging.info("Fetching/Loading keep_tags from YAML file")
    if deid_profile_path:
        keep_tags = fetch_and_load_keep_tags(deid_profile_path)
    else:
        # Fetch and load keep_tags from default deid.yaml profile
        default_deid_path = (
            Path(__file__).parent.parent
            / "utils"
            / "deid"
            / "cleans_safe_private_cleans_dates.yaml"
        )
        keep_tags = fetch_and_load_keep_tags(str(default_deid_path))
    logging.info("Fetching file info from Flywheel")
    fetch_file_info(api_key, output_dir, keep_tags, file_path, session_id, file_name)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Download header information from Flywheel and write it to a JSON file."
    )
    parser.add_argument("api_key", type=str, help="Flywheel API key")
    parser.add_argument(
        "--output_dir",
        type=str,
        default="tests/ground_truth/unsorted",
        help="Output directory for the JSON file",
    )
    parser.add_argument(
        "--deid_profile_path",
        type=str,
        help="Path to the deid.yaml profile to fetch keep_tags from",
        required=False,
    )
    parser.add_argument(
        "--file_path",
        type=str,
        help="Optional file path in Flywheel. Use this or session_id and file_name",
        required=False,
    )
    parser.add_argument(
        "--session_id",
        type=str,
        help="Optional session ID in Flywheel. Requires file_name if used",
        required=False,
    )
    parser.add_argument(
        "--file_name",
        type=str,
        help="Optional file name in Flywheel. Requires session_id if used",
        required=False,
    )

    args = parser.parse_args()

    # Simple validation to ensure either file_path or both session_id and file_name are provided
    if not args.file_path and not (args.session_id and args.file_name):
        parser.error(
            "You must provide either --file_path or both --session_id and --file_name."
        )
    else:
        main(
            api_key=args.api_key,
            output_dir=args.output_dir,
            deid_profile_path=args.deid_profile_path,
            file_path=args.file_path,
            session_id=args.session_id,
            file_name=args.file_name,
        )
